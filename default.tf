terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.122.0"
    }
  }
}

locals {
  cloud_id  = "b1g1ctcgoajdlehmj7po"
  folder_id = "b1gnqmitsufjgumfl482"
}

provider "yandex" {
  cloud_id = local.cloud_id
  folder_id = local.folder_id
  service_account_key_file = "C:/Users/grish/Desktop/my-awesome-projecty/authorized_key.json"
  zone = "ru-central1-a"
}