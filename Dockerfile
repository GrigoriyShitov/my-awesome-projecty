# Использование официального образа openjdk для сборки серверной части
FROM openjdk:17 AS build-server
WORKDIR /App

# Копирование проекта и восстановление зависимостей
COPY . .
WORKDIR ./backend
RUN mvn package

# Копирование исходного кода сервера и его сборка
COPY backend/ ./backend/
RUN mvn spring-boot:run

# Официальный образ Node.js для сборки клиентской части
FROM node:20.14.0 AS build-client
WORKDIR /App

# Копирование и установка зависимостей клиентской части
COPY frontend/package*.json ./frontend/
WORKDIR /App/frontend
RUN npm install

# Копирование исходного кода клиента и его сборка
COPY frontend/ ./
RUN npm run build

# Создание финального образа, объединяющего серверную и клиентскую части
FROM openjdk:17
WORKDIR /app

# Копирование результатов сборки сервера в новый контейнер
COPY --from=build-server /App/backend/target/*.jar /app/

# Копирование результатов сборки клиента в новый контейнер
COPY --from=build-client /App/frontend/build /app/static

# Определение порта, который будет слушать сервер
EXPOSE 8080

# Команда для запуска приложения
ENTRYPOINT ["java", "-jar", "*.jar"]