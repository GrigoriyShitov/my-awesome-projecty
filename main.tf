locals {
 bucket_name = "devops-site" 
 index= "index.html"
}

data "yandex_compute_image" "ubuntu_image" {
  family = "ubuntu-2204-lts"
}

resource "yandex_compute_instance" "vm-test1" {
  name = "test1"
  allow_stopping_for_update = true
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.id
    }
  }
 
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_terraform.id
    nat       = true
  }
 
  metadata = {
    user-data = "${file("meta.yml")}"
    ssh-keys = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPiJuJi0LdLi1el7XL3AeEZk96nIIrJxyshDP1SPKoVL grish@DESKTOP-VTCR9SO"
  }
 
}

 
resource "yandex_vpc_network" "network_terraform" {
  name = "net_terraform"
}
 
resource "yandex_vpc_subnet" "subnet_terraform" {
  name           = "sub_terraform"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network_terraform.id
  v4_cidr_blocks = ["192.168.15.0/24"]
}
