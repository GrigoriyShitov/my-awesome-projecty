// jest.config.js
module.exports = {
    testEnvironment: 'jest-environment-jsdom',
    // !
    extensionsToTreatAsEsm: ['.jsx'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
    transformIgnorePatterns: ['node_modules/(?!@babel-runtime|react|react-dom|react-router-dom)'],

  }