import { ReactComponent as GithubIcon } from 'assets/img/github.svg';
import SOVA from 'assets/img/SOVA.png';
import './styles.css';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <header>
      <nav className="container">
        <div className="dsmovie-nav-content">
        <Link to="/">
          <h1>GROWL</h1>
          </Link>
          <a href="https://gitlab.se.ifmo.ru/GrigoriyShitov/my-awesome-projecty">
            <div className="GROWL-contact-container">
            <img src={SOVA}  width={25} height={25} alt='Large Pizza' />
              <a className="GROWL-contact-link">FEEDBACK</a>
            </div>
          </a>
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
