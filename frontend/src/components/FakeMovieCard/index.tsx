import MovieScore from "components/MovieScore";
import { Link } from "react-router-dom";
import { Movie } from "types/movie";

type Props = {
    movie: Movie;
}

function FakeMovieCard({ movie }: Props) {
    if (movie.title == "Lichniy cabinet") {
        return (
            <div>
                <div className="row ">
                    <div className="dsmovie-card-bottom-container">
                        <h3>Личный кабинет</h3>
                        <div className="row ">
                            <div className="col ">
                                <div className="Card-of-client">Имя: Гарри Поттер</div>
                                <div className="Card-of-client">ID: 1</div>
                                <div className="Card-of-client">Баланс: 13500 галлеонов</div>
                            </div>
                            <div className="col-12">
                                <h3 >В пути</h3>
                                <div className="row justify-content-between">
                                    <div className="col-3">
                                        <div className="Card-of-client"> Номер транзакции</div>
                                        <div className="Card-of-client">#00004</div>
                                        <div className="Card-of-client">#00003</div>
                                        <div className="Card-of-client">#00002</div>
                                    </div>
                                    <div className="col-3">
                                        <div className="Card-of-client"> Статус</div>
                                        <div className="Card-of-client">В пути</div>
                                        <div className="Card-of-client">В пути</div>
                                        <div className="Card-of-client">В пути</div>
                                    </div>
                                    <div className="col-3">
                                        <div className="Card-of-client"> Местоположение совы</div>
                                        <Link to={``}>
                                            <div className="Card-of-client">На карте</div>
                                        </ Link>
                                        <Link to={``}>
                                            <div className="Card-of-client">На карте</div>
                                        </ Link>
                                        <Link to={``}>
                                            <div className="Card-of-client">На карте</div>
                                        </ Link>
                                    </div>
                                    <div className="col-3">
                                        <div className="Card-of-client"> Время выполнения</div>
                                        <div className="Card-of-client">0:00:22</div>
                                        <div className="Card-of-client">1:09:22</div>
                                        <div className="Card-of-client">1:29:57</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="dsmovie-card-bottom-container">
                        <h3>История транзакций</h3>
                        <div className="row align-items-center">
                            <div className="col-12">
                                <div className="row justify-content-between">
                                    <div className="col-3">
                                        <div className="Card-of-client"> Номер транзакции</div>
                                        <div className="Card-of-client">#00001</div>
                                    </div>
                                    <div className="col-3">
                                        <div className="Card-of-client"> Статус</div>
                                        <div className="Card-of-client">Завершена</div>
                                    </div>
                                    <div className="col-3">
                                        <div className="Card-of-client"> Сумма перевода</div>
                                        <div className="Card-of-client">80 g</div>
                                    </div>
                                    <div className="col-3">
                                        <div className="Card-of-client"> ID получателя</div>
                                        <div className="Card-of-client">3</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    else if (movie.title == "Oformit transaction") {
        movie.title = "Оформить транзакцию"
        return (
            <div>
                <div className="dsmovie-card-bottom-container">
                    <h3>{movie.title}</h3>
                    <Link to={`/form/${movie.id}`}>
                        <div className="btn btn-primary dsmovie-btn">начать операцию</div>
                    </ Link>
                </div>
            </div>
        );
    }
    else
        return (
            <div>
                <div className="dsmovie-card-bottom-container">
                    <h3>{movie.title}</h3>
                    <Link to={`/form/${movie.id}`}>
                        <div className="btn btn-primary dsmovie-btn">начать операцию</div>
                    </ Link>
                </div>
            </div>
        );
}

export default FakeMovieCard;