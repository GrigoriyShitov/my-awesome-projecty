import { Link, useNavigate } from "react-router-dom";
import { Movie } from 'types/movie';
import { useEffect, useState } from 'react';
import axios, { AxiosRequestConfig } from 'axios';
import { BASE_URL } from "utils/requests";
import './styles.css';
import { validateEmail } from "utils/validate";


type Props = {
    movieId: string;
};

function FormCard( { movieId } : Props) {

    const navigate = useNavigate();

    const[movie, setMovie] = useState<Movie>();

    useEffect(() => {
        axios.get(`${BASE_URL}/movies/${movieId}`)
             .then( response => {
                 setMovie(response.data);
             });
    }, [movieId]);

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {

        event.preventDefault();

        const email = (event.target as any).email.value;
        const score = (event.target as any).score.value;

        if(!validateEmail(email)){
            return;
        }

        const config: AxiosRequestConfig = {
            baseURL: BASE_URL,
            method: 'PUT',
            url: '/scores',
            data: {
                email: email,
                movieId: movieId,
                score: score
            }
        }

        axios(config).then(response => {
            navigate("/");
        });
    }

    

    return (
        <div className="dsmovie-form-container">
            <div className="dsmovie-card-bottom-container">
                <h3>Оформить транзакцию</h3>
                <form className="dsmovie-form" onSubmit={handleSubmit}>
                    <div className="form-group dsmovie-form-group">
                        <label htmlFor="score">Введите тип операции</label>
                        <select className="form-control">
                            <option>Проведение транзакции со своим счетом</option>
                            <option>Проведение транзакции с чужим счетом</option>
                        </select>
                    </div>
                    <div className="form-group dsmovie-form-group">
                        <label htmlFor="score">Тип операции</label>
                        <select className="form-control">
                            <option>Запросить</option>
                            <option>Положить</option>
                        </select>
                    </div>
                    <div className="form-group dsmovie-form-group">
                        <label htmlFor="score">ФИО получателя</label>
                        <input type="string" className="form-control" />
                    </div>
                    <div className="form-group dsmovie-form-group">
                        <label htmlFor="email">Введите сумму </label>
                        <input type="number" className="form-control" min='1' max='2000'/>
                    </div>
                    <Link to={`/1`}>
                    <button type="submit" className="btn btn-primary dsmovie-btn mt-3">Спасти и сохранить
                    </button>
                    </Link>
                </form >


            </div >
        </div >
    );
}
export default FormCard;