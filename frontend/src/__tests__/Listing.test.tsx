test.todo('получение приветствия')

// import React from 'react';
// import { render, fireEvent, waitFor } from '@testing-library/react';
// import  Listing  from '../pages/Listing';
// import axios from 'axios';
// import MovieCard from '../components/MovieCard';
// import Pagination from '../components/Pagination';
// import { MoviePage } from '../types/movie';

// describe('Listing', () => {
//   it('renders the Pagination component', async () => {
//     const { getByText } = render(<Listing />);
//     await waitFor(() => getByText('Pagination'));

//     expect(getByText('Pagination')).toBeInTheDocument();
//   });

//   it('renders the MovieCard components', async () => {
//     const { getAllByRole } = render(<Listing />);
//     await waitFor(() => getAllByRole('button'));

//     expect(getAllByRole('button')).toHaveLength(12);
//   });

//   it('calls the axios get request when the component mounts', async () => {
//     const axiosGet = jest.spyOn(axios, 'get');
//     render(<Listing />);
//     await waitFor(() => expect(axiosGet).toHaveBeenCalledTimes(1));
//   });

//   it('calls the axios get request when the page number changes', async () => {
//     const axiosGet = jest.spyOn(axios, 'get');
//     const { rerender } = render(<Listing />);
//     rerender(<Listing />);
//     await waitFor(() => expect(axiosGet).toHaveBeenCalledTimes(2));
//   });
// });
