package com.devsuperior.dsmovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsmovieApplication {
	
	public int add(int a, int b){
        return a + b;
	}	
		public static void main(String[] args) {
		SpringApplication.run(DsmovieApplication.class, args);
	}

}
