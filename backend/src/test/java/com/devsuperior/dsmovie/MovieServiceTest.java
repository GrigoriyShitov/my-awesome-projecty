package com.devsuperior.dsmovie;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;

import org.junit.jupiter.api.Test;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.devsuperior.dsmovie.dto.MovieDTO;
import com.devsuperior.dsmovie.entities.Movie;
import com.devsuperior.dsmovie.services.MovieService;
import com.devsuperior.dsmovie.repositories.MovieRepository;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.data.domain.Pageable;

import static org.mockito.Mockito.when;



@ExtendWith(MockitoExtension.class)
class MovieServiceTest {

    @Mock
    private MovieRepository repository;

    @InjectMocks
    private MovieService movieService;

    @Test
    void testFindAll() {
        // Arrange
        Pageable pageable = PageRequest.of(0, 10);
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1L, "Movie 1", 5.0, 2, ""));
        movies.add(new Movie(1L, "Movie 2", 5.0, 2, ""));
        Page<Movie> page = new PageImpl<>(movies, pageable, movies.size());

        when(repository.findAll(pageable)).thenReturn(page);

        // Act
        Page<MovieDTO> result = movieService.findAll(pageable);

        // Assert
        assertNotNull(result);
        assertEquals(2, result.getContent().size());
        assertEquals("Movie 1", result.getContent().get(0).getTitle());
        assertEquals("Movie 2", result.getContent().get(1).getTitle());
    }
}







