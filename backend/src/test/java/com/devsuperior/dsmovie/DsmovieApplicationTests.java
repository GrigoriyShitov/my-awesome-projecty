package com.devsuperior.dsmovie;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DsmovieApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Test
    public void add(){
        DsmovieApplication calculator = new DsmovieApplication();
        int expected =15;
        int result = calculator.add(10,5);
        assertEquals(expected, result);
        int expected2 =150;
        int result2 = calculator.add(100,50);
        assertEquals(expected2, result2);
    }
}
